---
layout: markdown_page
title: "People Operations"
---

- [Reaching People Operations](#reach-peopleops)
- [Office addresses](#addresses)
- [Setting up new contracts](#new-contracts)
   - [Using HelloSign](#hellosign)
- [Processing changes](#processing-changes)
- [Using BambooHR](#bamboohr)
   - [Adding a new team member to BambooHR](#admin-bamboo)
   - [Using BambooHR as a new team member](#new-member-bamboo)
- [Managing the PeopleOps onboarding tasks](#manage-onboarding-tasks)   
- [Administrative details of benefits for US-based employees](#benefits-us)
   - [401k](#401k)
- [Using TriNet](#using-trinet)
   - [Add new hires to TriNet](#trinet-process)
   - [Making changes in TriNet](#changes-trinet)
- [Returning property to GitLab](#returning-property)
- [Using RingCentral](#ringcentral)
- [Paperwork people may need to obtain mortgage in the Netherlands](#dutch-mortgage)

## Reaching People Operations<a name="reach-peopleops"></a>

To reach People Operations, please post an issue on our [internal issue tracker](https://dev.gitlab.org/gitlab/organization/issues/)
and add the 'PeopleOps' label, or send an email to the People Operations group (see the "GitLab Email Forwarding" google doc), or ping an individual
member of the People Operations team, as listed on our [Team page](https://about.gitlab.com/team/).

## Office addresses<a name="addresses"></a>

- For the SF office, see our [visiting](https://about.gitlab.com/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](www.addpost.nl) to scan our mail and send it along to a physical address upon request. The scans are sent via email to the email alias listed in the "GitLab Email Forwarding" google doc.


## Setting up new hiring contracts<a name="new-contracts"></a>

New team hire contracts are found on the [Contracts](https://about.gitlab.com/handbook/contracts/) page, including instructions on how to set up new contracts.
Templates for the contracts are set up in BambooHR. You can download the template from the "Files" tab and work on the contract using Google Drive. Here you can work together if you need help and download the PDF for HelloSign.


### Using HelloSign<a name="hellosign"></a>

When we need [contracts to be signed](https://about.gitlab.com/handbook/#signing-legal-documents) we use [HelloSign](https://hellosign.com).
Follow these steps to send out a signature request.

1. Choose who needs to sign. (just me, me & others, or just others)
1. Upload your document that needs signing
1. Enter the names of the people that need to sign
1. With more than one signature required, assign signing order (for contracts or offer letters, always have the GitLab Signatory sign first)
1. Add the People Ops team to the cc
1. Click on "Prepare Docs for signing"
1. Drag & Drop the Signature and Date fields to the corresponding empty spaces in the document (you can select the signee with the pop up screen)
1. Add a title and a message for the recipient. For contract use for example: "Dear [...], You can sign [document type] with HelloSign. Once you've signed you will receive a copy by email. If you have any questions, feel free to reach out"
1. Request Signature.

Once you've sent out the document you will receive email notifications of the progress and a copy of the signed document after all parties have signed.

## Processing changes<a name="processing-changes"></a>

Any changes to a team member’s status, classification, promotion/demotion, pay increase/decrease and
bonus needs to be communicated via email to People Ops by the team member's manager or CEO. People Ops will then enter
the changes in BambooHR under the Jobs Tab in the member’s profile; then file the email in the Offers
and Contracts Folder in the Document Section.

Further changes may need to be [processed in TriNet](#changes-trinet) or Savvy to fully process the change. People Ops
is responsible for seeing the change through to completion. Once completed, People Ops
sends an email to the person reporting / requesting the change (member's manager or CEO)
to confirm this.


## Using BambooHR<a name="bamboohr"></a>

We’re using [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action:  

1. document what the process will be regarding this action (e.g. "need information X") for
_new_ team members. (Recall that we strive to change processes by documenting them, not vice versa).
1. make sure _that it is necessary_  for the entire team to act, or whether the
work can be done by People Ops (even if this is tedious to do for People Ops, it
is preferred, so as not to burden the team),
1. make sure that the call to action is OK-ed by management,
1. and check the steps to be taken by testing them with a single team member or a
test account that does not have admin privileges.
1. in the note to the team, point to the documentation created in step 1, explain the need and
the ask, and who to turn to in case of questions.

### Adding a new team member to BambooHR <a name="admin-bamboo"></a>

After People Ops is requested to make a contract or offer letter for a potential hire,
as described on our [Hiring](/handbook/hiring/) page, follow these steps:

1. Download the correct template from the files page on BambooHR
1. Edit the contract or offer letter to include the new hire information.
1. Send out the document to be signed using HelloSign, \cc people ops in this process. (document HelloSign first)
1. Once the document has been signed, create a BambooHR employee file:
   1. Go to the BambooHR Dashboard
   1. Click on the top right on “Add employee”
   1. Enter the following fields: First name, Last name, Gender, Hire date, Employment status, Job information, Pay rate, Salary for employees, or select another option.
   1. Turn “self service” on for the employee to enter the following info: Address, Phone, DOB. private email.
1. Save the signed contract under the Documents tab in the folder “0.Signed Contract/Offer Letter”.
1. As noted in the onboarding checklist, on the start date of the employee send out the "password reset email" under
the settings option in the top right of the profile (next to "request a change")
**Note** This link is only valid for 24 hours.

### Using BambooHR as a new team member<a name="new-member-bamboo"></a>

Team members can keep their information in BambooHR updated using self-service:

1. Go to the BambooHR link that you should have received by email
1. Change the password to make it secure (see the handbook section on [secure passwords](https://about.gitlab.com/handbook/security/))
1. Login with your new credentials
1. Keep your credentials stored in your 1Password Vault and keep the info in BambooHR updated (e.g. change address when moving)

## Managing the PeopleOps onboarding tasks <a name="manage-onboarding-tasks"></a>

Here are some useful guidelines to set up everything for our new team members with the onboarding issue.
If Google Sheets or Docs are mentioned, these will be shared with you on Google Drive.

- **Add team member to availability calendar**
Make a new "all-day" event on the day the new team member starts "[Name] joining
as [Job title]". Make sure to select the calendar as GitLab Availability and not your own.
- **Give team member access to the GitLab availability calendar**
Go to your calendar window, under my calendars move your cursor over the calendar
and click the dropdown triangle. Select calendar settings and go to "Share this
calendar" in the top of the window. Enter the GitLab email address and scroll
down to set the permission setting to "make changes to events" Then save in the
lower left corner.
- **Add blank entry to team page**
Login to [Gitlab.com](www.gitlab.com) and go to the www-gitlab-com project. In
the left menu click "Files" and select the folder called "source". Continue by
clicking the folder "data" and select the file called team.yml. In the top right
corner you can click "edit" to make edits to the code. Scroll all the way down
and copy-paste the code of the previous blank entry to the team page. Edit the
fields to the right info of the new hire and find the right job description on
the [Jobs](https://about.gitlab.com/jobs/) page.
 **Note** _This can be tricky, so if you run into trouble reach out to some of
your awesome colleagues in the #questions (channel) on Slack_
- **Add entry to Team Call agenda**
Open the Team Agenda google doc, and on the starting day add an Agenda item:
"[Hiring manager name]: Welcome [name new team member] joining as [job title]"
as the first item on the agenda
- **Invite to team meeting and GitLab 101 meeting**
Go to the team call meeting on the starting date of the team member and the next
scheduled GitLab 101 in the Availability calendar. Click on "edit event" to open.
On the right enter the team member's GitLab email address in the  "add guests"
section and click save. When asked select "all events" to add to all scheduled
meetings and "send" out the invitation.
- **Order business cards**
Go to the "Notes" field in BambooHR and enter the info needed for the Business Cards.
Once every week/few days or how often needed, run the "Business Cards order" report in the Reports - My Reports menu of BambooHR.
Email our partner to order new business cards and add the info for the cards to
be ordered. Include the address of the team member (found in BambooHR) and inform
our partner to _**ship the cards directly to that address**_. Double check the
info that is sent with the preview before approving the print.
- **Add team member to Expensify (only with employees)**
Login to [Expensify](https://www.expensify.com/signin) and go to "Admin" in the
top menu. Select the right policy based upon the entity that employs the new team
member. Select "People" in the left menu. Select "Invite" and add the GitLab email.
Edit the message to be sent for the employee. Click "invite".
- **Add team member to Beamy**
Login in to access the settings for the [Beam](https://suitabletech.com/accounts/login/).
In the top menu move your cursor over the blue login button. Go to "Manage your beams".
Click on "manage" in the lower left corner. Enter the GitLab email and scroll down
to find the newly addedd email. Check the box for "Auto connect".
- **Add team member into the Austin info sheets**
Add the team member's name to the Travel and Lodging Google sheets to make sure they enter flight details and they are assigned a room.
- **Add team member to our Egencia platform**
Log into Egencia and go to the menu option "manage users". Choose "new user account" and fill in the passport name of the new team member.
As username choose the same handle as on our dev page. Enter the GitLab email address and uncheck the newsletter box.
Lastly assign the department in which the new team member is working.

## Administrative details of benefits for US-based employees <a name="benefits-us"></a>

### 401k<a name="401k"></a>

1. You are eligible to participate in GitLab’s 401k as of the 1st of the month after your hire date.  
1. You will receive a notification on your homepage in TriNet Passport once eligible,
if you follow the prompts it will take you to the Transamerica website https://www.ta-retirement.com/
or skip logging in to TriNet Passport and go directly to https://www.ta-retirement.com/
after the 1st of the month after your hire date.
1. Once on the home page of https://www.ta-retirement.com/ go to "First Time User Register Here".  
1. You will be prompted for the following information
   1. Full Name
   1. Social Security Number
   1. Date of Birth
   1. Zip Code  
1. Once inside the portal you may elect your annual/pay-period contributions, and Investments.

## Using TriNet<a name="using-trinet"></a>

### Entering New Hires into TriNet<a name="trinet-process"></a>

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.
1. Enter all of the necessary information:
    * Company name autopopulates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gener
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.
1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).
1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.

### Making changes in TriNet <a name="changes-trinet"></a>

#### Add a New Location

1. Go to HR Passport homepage
1. Click Find
1. Click Find Location.
1. When search field appears, leave blank and click Search.
1. Click on Add location.
1. Complete location information. For a remote location, enter the location (ex. WA remote) in all fields except city, state and zip.
1. Click Add.

#### Transfer Employee to Different Location

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.


## Returning property to GitLab<a name="returning-property"></a>

As part of [offboarding](https://about.gitlab.com/handbook/offboarding/), any
GitLab property needs to be returned to GitLab. GitLab
will pay for the shipping either by PeopleOps sending a FedEx shipping slip or it can be returned
by another mutually agreed method. If property is not returned, GitLab reserves the
right to use a creditor company to help retrieve the property.

## Using RingCentral<a name="ringcentral"></a>

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
want to make changes.
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
the current settings which show all the people and numbers that are alerted when the listed User's
number is dialed.
- Add the new forwarding number (along with a name for the number), and click Save.

## Paperwork people may need to obtain mortgage in the Netherlands<a name="dutch-mortgage"></a>

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring". 
This document describes your salary and states that your employer expects to continue to employ 
you after the contract expires (assuming the performance of the employee doesn't degrade). 
This document has to be filled in by hand, preferably using blue ink, and must be signed 
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed) 
must be supplied that states the employer has no stamp. While the language of these 
documents doesn't matter, the use of Dutch is preferred.

Employees also have to provide a copy of a payslip that clearly states not only their 
monthly salary but also their annual salary. These numbers must match the numbers on 
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require 
you to provide information about your financial status, usually in the form of a report/screenshot 
of your total financial status (including any savings you have). The requirements for 
this seem to vary a bit between mortgage providers.
